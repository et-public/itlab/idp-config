##
## Velocity Template for DisplayTermsOfUsePage view-state
##
## Velocity context will contain the following properties :
##
## encoder - HTMLEncoder class
## flowExecutionKey - SWF execution key (this is built into the flowExecutionUrl)
## flowExecutionUrl - form action location
## flowRequestContext - Spring Web Flow RequestContext
## request - HttpServletRequest
## response - HttpServletResponse
## rpUIContext - context with SP UI information from the metadata
## termsOfUseId - terms of use ID to lookup message strings
## environment - Spring Environment object for property resolution
#set ($serviceName = $rpUIContext.serviceName)
#set ($rpOrganizationLogo = $rpUIContext.getLogo())
##
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>#springMessageText("${termsOfUseId}.title", "Terms of Use")</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=windows-1252">
    <link rel="icon" href="$assetsfavicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="$assets/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="$assets/css/bootstrap.min.css">
    <link href="$assets/css/su-identity.css" rel="stylesheet">
    <script src="$assets/js/jquery-1.11.1.min.js"></script>

    <!-- bootstrap -->
    <script src="$assets/js/bootstrap.min.js"></script>

    <!--[if lt IE 9]>
    <script src="$assets/js/html5.js"></script>
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="$assets/css/ie8.css" />
    <![endif]-->
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="$assets/css/ie7.css" />
    <![endif]-->

    <script src="$assets/js/login.js"></script>
    <!-- login CSS -->
    <link rel="stylesheet" href="$assets/css/login.css">

    <meta http-equiv="refresh" content="20;url=$flowExecutionUrl&_eventId_proceed=1">
  </head>

  <body>
    <div id="su-wrap">
      <div id="su-content">
        <div id="brandbar">
          <div class="container">
            <a href="http://www.stanford.edu"> <img src="$assets/images/brandbar-stanford-logo@2x.png" alt="Stanford University" width="153" height="22"> </a>
          </div>
        </div>

        <div id="header">
          <div class="container">
            <div style="margin: 1em 10px 0 0;">
              <h2 class="pagetitle">
                <title>#springMessageText("${termsOfUseId}.title", "Terms of Use")</title>
              </h2>
            </div>
          </div>
        </div>

        <div id="content">
          <div class="container">

            <div class="row">
              <div class="col-md-8 bg-warning">

                <div id="tou-content">
                  #springMessageText("${termsOfUseId}.text", "Terms of Use Text...")
                </div>
              </div>
            </div>

            <div class="row">
              <form action="$flowExecutionUrl" method="post" >
                <div class="col-md-1">
                  <input type="submit" name="_eventId_TermsRejected" value="#springMessageText("idp.terms-of-use.reject", "Refuse")" style="margin-right: 30px;">
                </div>
                <div class="col-md-offset-1 col-md-4">
                  <input id="accept" type="checkbox" name="_shib_idp_consentIds" value="$encoder.encodeForHTML($termsOfUseId)" required>
                  <label for="accept">#springMessageText("idp.terms-of-use.accept", "I accept the terms of use")</label>
                  #if ($requireCheckbox)
                    <p class="form-error">#springMessageText("idp.terms-of-use.required", "Please check this box if you want to proceed.")</p>
                  #end
                </div>
                <div class="col-md-offset-1 col-md-1">
                  <input type="submit" name="_eventId_proceed" value="#springMessageText("idp.terms-of-use.submit", "Submit")">
                </div>
              </form>
            </div>

            <div class="row">
              <div class="col-md-12">
                <p class="fineprint">
                  This is not the real Stanford WebLogin system.  This is the
                  IT Lab Authentication System that we use for testing.
                </p>
              </div>
            </div>

          </div>
        </div>

      </div>
    </div>

    <div id="global-footer">
      <div class="container">
        <div class="row">
          <div class="col-xs-6 col-sm-2" id="bottom-logo"> <a href="http://itlab.stanford.edu"> <img width="105" height="49" alt="IT Lab" src="$assets/images/bunsen-beaker-2.png"> </a> </div>
          <div class="col-xs-6 col-sm-10" id="bottom-text">
            <ul>
              <li class="home"><a href="http://www.stanford.edu">SU Home</a></li>
              <li class="home"><a href="http://uit.stanford.edu">UIT Home</a></li>
              <li class="search-stanford"><a href="http://stanford.edu/search/">Search Stanford</a></li>
            </ul>
          </div>
          <div class="clear"></div>
          <p class="copyright vcard col-sm-10">&copy; <span class="fn org">Stanford University</span>.&nbsp; <span class="adr"> <span class="locality">Stanford</span>, <span class="region">California</span> <span class="postal-code">94305</span></span>. <span id="termsofuse"><a data-ua-label="global-footer" data-ua-action="copyright-complaints" class="su-link" href="http://www.stanford.edu/group/security/dmca.html">Copyright Complaints</a>&nbsp;&nbsp;&nbsp;<a data-ua-label="global-footer" data-ua-action="trademark-notice" class="su-link" href="https://adminguide.stanford.edu/chapter-1/subchapter-5/policy-1-5-4">Trademark Notice</a></span></p>
        </div>
      </div>
    </div>

  </body>
</html>
